

public interface EmployeeService {
	
	
	public boolean createEmployee(String job,int wh);
	
	public boolean saveEmployee(Employee emp);
	
	public double getEmployeeSalary(int empId);

}
