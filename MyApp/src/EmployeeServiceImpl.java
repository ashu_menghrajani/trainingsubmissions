
public class EmployeeServiceImpl implements EmployeeService {

	static int lastEid = 995;

	Configuration c = new Configuration();
	SalaryCalculator sc = new SalaryCalculator();

	@Override
	public boolean createEmployee(String job, int wh) {

		Employee e = new Employee(job, wh);

		e.setId(EmployeeServiceImpl.lastEid++);

		return saveEmployee(e);
	}

	@Override
	public boolean saveEmployee(Employee emp) {

		DBConnector db = c.getDBConnector(emp.getId());
		return db.persist(emp);
	}

	@Override
	public double getEmployeeSalary(int empId) {
		DBConnector db = c.getDBConnector(empId);
		Employee emp = db.getEmployee(empId);
		return sc.getEmployeeSalary(emp);
	}

}
