

import java.util.HashMap;

public class MySqlConnector implements DBConnector {
	
	static HashMap<Integer,Employee> hm = new HashMap<>();

	@Override
	public boolean persist(Employee emp) {
		
		System.out.println("Storing In MySqlDB");
		
		if(MySqlConnector.hm.containsKey(emp.getId()))		
		return false;
		else MySqlConnector.hm.put(emp.getId(), emp);
		return true;
	}

	@Override
	public Employee getEmployee(int empId) {
		System.out.println("Fetching From MySqlDB");
		
		if (MySqlConnector.hm.get(empId)!=null)
		return MySqlConnector.hm.get(empId);
		else return null;
	}

}
