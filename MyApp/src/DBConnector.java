

public interface DBConnector {
	
	public boolean persist(Employee emp);
	
	public Employee getEmployee(int empId);

}
