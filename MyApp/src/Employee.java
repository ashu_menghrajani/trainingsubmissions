

public class Employee {
	
	private int id;
	
	private String job;
	
	private int workingHours;
	
	

	public Employee(String job, int workingHours) {
		super();
		this.job = job;
		this.workingHours = workingHours;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public int getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(int workingHours) {
		this.workingHours = workingHours;
	}
	
	
	

}
