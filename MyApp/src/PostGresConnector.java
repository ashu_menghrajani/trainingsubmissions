

import java.util.HashMap;

public class PostGresConnector implements DBConnector {

	static HashMap<Integer,Employee> hm = new HashMap<>();

	@Override
	public boolean persist(Employee emp) {
		
		System.out.println("Storing In PostgresDB");		
		if(PostGresConnector.hm.containsKey(emp.getId()))		
		return false;
		else PostGresConnector.hm.put(emp.getId(), emp);
		return true;
	}

	@Override
	public Employee getEmployee(int empId) {
		
		System.out.println("Fetching From Postgres");
		
		if (PostGresConnector.hm.get(empId)!=null)
		return PostGresConnector.hm.get(empId);
		else return null;
	}

}
